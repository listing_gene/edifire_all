<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="recruit_content">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>Recruit</h1>
                    <div class="in_tx">
                        <h2>アルバイト採用情報</h2>
                        <p>オーナー様向けパーティー、B&B、各種イベントスタッフを行なっていただく人材を募集中です。</p>
                        <img src="img/partjob_pc.png" class="pc" alt="">
                        <img src="img/partjob.jpg" class="s" alt="">
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="frame">
                    <div class="in_tx02">
                        <h3>募集概要</h3>
                        <dl class="rc_detail_text">
                            <dt>募集人数</dt>
                            <dd>10名</dd>
                            <dt>業務内容</dt>
                            <dd>定期的に開催する顧客向けParty、B&B事業でのイベントの運営業務</dd>
                            <dt>応募資格</dt>
                            <dd>●学生OK<br>●週２〜　OK<br>●年齢・学歴不問</dd>
                            <dt>賞与</dt>
                            <dd>9月、3月(賞与査定により決定）</dd>
                            <dt>待遇</dt>
                            <dd>交通費全額支給</dd>
                            <dt>時給</dt>
                            <dd>1,000円〜2,000円</dd>
                            <dt>勤務時間</dt>
                            <dd>17:00〜22:00</dd>
                            <dt>勤務地</dt>
                            <dd>関東近郊（イベント開催場所）</dd>
                            <dt>お問い合わせ</dt>
                            <dd>〒106-0032<br>
                                東京都港区六本木5-2-1　ほうらいやビル5階<br>
                                <a href="tel:03-5544-8522">TEL：03-5544-8522</a><br>
                                <a href="mailto:info@edifire.co.jp">e-mail：info@edifire.co.jp</a><br>
                                EDIFIRE アルバイト採用担当宛<br>
                                ●HPでアルバイト採用情報を見たとお伝えいただけるとスムーズです。
                                すぐに面接の予約受付を進めさせていただきます。</dd>
                        </dl>
                    </div>
                    <!-- <a class="job" href="#">応募はこちらから</a> -->
                </div>
            </div>
            <div class="inner">
                <div class="conte_bg03 frame recruit_inbottom">
                    <p>各ページにて詳細をご確認いただけます。</p>
                    <div class="recruit_list">
                        <a class="recruit_detail" href="rc_dt_01.php">セールス採用</a>
                        <a class="recruit_detail" href="rc_dt_02.php">エンジニア採用</a>
                        <a class="recruit_detail" href="rc_dt_03.php">管理スタッフ採用</a>
                        <a class="recruit_detail" href="rc_dt_04.php">アルバイト採用</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>