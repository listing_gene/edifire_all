<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
  <meta name="format-detection" content="telephone=no">
  <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
  <title>株式会社EDIFIRE</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/header-footer.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <link href="css/reboot.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200;400&display=swap" rel="stylesheet">
</head>

<body>
  <div id="LOADER-BG">
    <div class="LOADER-EC"><img class="load" src="./img/logo_bg.png" alt=""></div>
  </div>
  <?php include('./header.html'); ?>
  <main>
    <div class="mv">
      <div class="mv_cc">
        <p>Act with one</p>
        <p>philosophy</p>
        <!-- <p>私の生きる原動力は、<br class="sp">
          ”人の役に立ちたい”ただそれだけです。</p> -->
      </div>
      <!-- <a href="about.php">ENTRY</a> -->
    </div>
  </main>
  <?php include('./footer.html'); ?>
  <script>
    $(function() {
      $('.btn-trigger').on('click', function() {
        $(this).toggleClass('active');
        return false;
      });
    });
    $(function() {
      var $btn = $('.btn-trigger');
      $('.hamberger').click, $btn.click(function() {
        $(this).toggleClass('open');
        if ($(this).hasClass('open')) {
          $('#mask').addClass('open'),
            $('.hamberger').addClass('open');
        } else {
          $('#mask').removeClass('open'),
            $('.hamberger').removeClass('open');
        }
      });
    });
  </script>
  <script>
    $(window).on('load', function() {
      $("#LOADER-BG").delay(2000).fadeOut(1300);
    });
  </script>
</body>

</html>