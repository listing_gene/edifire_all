<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200&display=swap" rel="stylesheet">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="service_content">
            <div class="inner">
                <div class="conte_bg02 frame">
                    <h1>Service</h1>
                    <h2>事業内容</h2>
                    <div class="bc_wrap">
                        <p>不動産投資に関するコンサルティング</p>
                        <div class="bc">
                            <img src="./img/BC_01_01.svg" alt="">
                            <p>安心できる資産</p>
                            <p>不動産投資の始め方から、あなたに合わせた資産創りをご提案致します。</p>
                        </div>
                        <div class="bc">
                            <img src="./img/BC_01_02.svg" alt="">
                            <p>所有と運営</p>
                            <p>不動産の大きな役割として”所有と運営”のうち、所有されているご資産を当社が運営。最大限の価値を引き出すトータルサポート。
                            </p>
                        </div>
                    </div>
                    <div class="bc_wrap inline">
                        <p>不動産流通プラットフォームの運営</p>
                        <div class="bc">
                            <img src="./img/BC_02_01.svg" alt="">
                            <p>情報発信基地として</p>
                            <p>不動産投資の始め方から、あなたに合わせた資産創りをご提案致します。</p>
                        </div>
                        <div class="bc">
                            <img src="./img/BC_02_02.svg" alt="">
                            <p>スピーディな対応</p>
                            <p>プラットフォーム上のオーナーネットワークを活かし、不動産売買をしっかりサポート。
                            </p>
                        </div>
                    </div>
                    <div class="bc_wrap inline">
                        <p>リノベーションに関する<br class="sp">デザイン、設計、請負</p>
                        <div class="bc">
                            <img src="./img/BC_03_01.svg" alt="">
                            <p>最大限の付加価値</p>
                            <p>多彩なデザイン、ナチュラルな感性や創造性、技術を駆使して生まれ変わる資産創り。</p>
                        </div>
                        <div class="bc">
                            <img src="./img/BC_03_02.svg" alt="">
                            <p>デザインファームとして</p>
                            <p>モノや空間をデザイン、プロデュースして人と人とのつながりを大切にします。</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="inner">
                <div class="conte_bg frame">
                    <!-- <h1>Service</h1> -->
                    <h2>EDIFIREが<br>
                        オーナーのためにしていること</h2>
                    <div class="in_tx">
                        <h3>01</h3>
                        <div class="service_text">
                            <p>アンバサダーマーケティング</p>
                            <p>実績や経験の少ない者がアウトバンドセールスでの顧客開拓をするのではなく、顧客の付加価値を高め、顧客満足重視、口コミ重視で顧客開拓をしていきます。</p>
                        </div>
                    </div>
                    <div class="in_tx left">
                        <h3>02</h3>
                        <div class="service_text">
                            <p>不動産のサブスクリプションモデル</p>
                            <p>ユーザー様であれば定額で不動産サービスを受けられる不動産のサブスクリプションモデルを構築しています。一度限りの付き合いになる事が多い不動産取引ですが、スマホをベースにテクノロジーを駆使して、価値あるコンテンツを提供してます。</p>
                        </div>
                    </div>
                    <div class="in_tx">
                        <h3>03</h3>
                        <div class="service_text">
                            <p>受託販売プラットフォーム</p>
                            <p>アウトバンドセールスではなく、信頼関係重視のインバウンドセールスで顧客開拓をして、不動産投資のプラットフォームを形成することにより、「いちげんさんお断り」の料亭と共通するアプローチになります。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="inner">
            <div class="conte_bg02 frame">
                <h2>事業内容</h2>
                <div class="bc_wrap">
                    <p>不動産投資に関するコンサルティング</p>
                    <div class="bc">
                        <img src="./img/BC_01_01.svg" alt="">
                        <p>安心できる資産</p>
                        <p>不動産投資の始め方から、<br class="pc">
                            あなたに合わせた<br class="pc">
                            資産創りをご提案致します。</p>
                    </div>
                    <div class="bc">
                        <img src="./img/BC_01_02.svg" alt="">
                        <p>所有と運営</p>
                        <p>不動産の大きな役割として<br class="pc">
                            ”所有と運営”のうち、<br class="pc">
                            所有されているご資産を当社が運営。<br class="pc">
                            最大限の価値を引き出すトータルサポート。</p>
                    </div>
                </div>
                <div class="bc_wrap inline">
                    <p>不動産流通プラットフォームの運営</p>
                    <div class="bc">
                        <img src="./img/BC_02_01.svg" alt="">
                        <p>情報発信基地として</p>
                        <p>不動産投資の始め方から、<br class="pc">
                            あなたに合わせた<br class="pc">
                            資産創りをご提案致します。</p>
                    </div>
                    <div class="bc">
                        <img src="./img/BC_02_02.svg" alt="">
                        <p>スピーディな対応</p>
                        <p>プラットフォーム上の<br class="pc">
                            オーナーネットワークを活かし、<br class="pc">
                            不動産売買をしっかりサポート。</p>
                    </div>
                </div>
                <div class="bc_wrap inline">
                    <p>リノベーションに関するデザイン、設計、請負</p>
                    <div class="bc">
                        <img src="./img/BC_03_01.svg" alt="">
                        <p>最大限の付加価値</p>
                        <p>多彩なデザイン、ナチュラルな感性や<br class="pc">
                            創造性、技術を駆使して<br class="pc">
                            生まれ変わる資産創り。</p>
                    </div>
                    <div class="bc">
                        <img src="./img/BC_03_02.svg" alt="">
                        <p>デザインファームとして</p>
                        <p>モノや空間をデザイン、<br class="pc">
                            プロデュースして<br class="pc">
                            人と人とのつながりを大切にします。</p>
                    </div>
                </div>
            </div>
        </div> -->
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>