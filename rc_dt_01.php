<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="recruit_content">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>Recruit</h1>
                    <div class="in_tx">
                        <h2>セールス採用情報</h2>
                        <p>EDIFIREは、不動産業界のセールスプロセスに変革をもたらし、ユーザーファーストなサービスを提供し、<br class="pc">
                            顧客から最も信頼される会社となるために、私たちのミッションに共感を持っていただけるパートナーを求めております。</p>
                        <h3>募集概要</h3>
                        <dl class="rc_detail_text">
                            <dt>募集職種</dt>
                            <dd>セールス&マーケティング<br>
                                （不動産投資を考えている方へのコンサルティング営業）</dd>
                            <dt>こんな人におすすめ</dt>
                            <dd>大手企業、公務員で転職や起業を考えている方<br>
                                ※EDIFIREアンバサダーからのエージェント登用制度あり</dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="frame">
                    <div class="in_tx02">
                        <h3>待遇、その他</h3>
                        <dl class="rc_detail_text">
                            <dt>雇用形態</dt>
                            <dd>正社員、契約社員、バイト、業務委託</dd>
                            <dt>給与</dt>
                            <dd>4半期ごとのパフォーマンスによって決定</dd>
                            <dt>モデル給与</dt>
                            <dd>セールス　月給50万　年収950万（インセンティブ含む）<br>
                                スタッフ　月給28万　年収450万（賞与含む）<br>
                                ※採用時の給与に関しては、職務経歴・面接結果等を参考にさせていただきます。</dd>
                            <dt>賞与</dt>
                            <dd>9月、3月(賞与査定により決定）</dd>
                            <dt>諸手当</dt>
                            <dd>交通費、住宅補助（港区に限り家賃の5万円まで）保育園補助(認可外の場合)</dd>
                            <dt>勤務地</dt>
                            <dd>東京都港区東麻布3-4-18（麻布十番駅から徒歩2分）</dd>
                            <dt>勤務時間</dt>
                            <dd>フレックスタイム制（1日8時間、コアタイムは11:00〜17:00）</dd>
                            <dt>休日</dt>
                            <dd>本人希望の平日1日、日曜日、祝祭日、年末年始、休暇慶弔、有給休暇</dd>
                            <dt>各種制度</dt>
                            <dd>エクストリーム出社制度、EDIFIREシエスタ、厚生年金保険、健康保険、雇用保険、住宅補助、インセンティブ（ストックオプション制度あり）</dd>
                            <dt>その他</dt>
                            <dd>どの職種に関しても1-3ヶ月の試用期間を設ける場合が有ります。</dd>
                            <dt>連絡先</dt>
                            <dd>〒106-0032<br>
                                東京都港区六本木5-2-1　ほうらいやビル5階<br>
                                <a href="tel:03-5544-8522">TEL：03-5544-8522</a><br>
                                <a href="mailto:info@edifire.co.jp">e-mail：info@edifire.co.jp</a></dd>
                        </dl>
                    </div>
                    <a class="job" href="https://www.secure-cloud.jp/sf/1484650082bFEBSXBa">応募はこちらから</a>
                </div>
            </div>
            <div class="inner">
                <div class="conte_bg03 frame recruit_inbottom">
                    <p>各ページにて詳細をご確認いただけます。</p>
                    <div class="recruit_list">
                        <a class="recruit_detail" href="rc_dt_01.php">セールス採用</a>
                        <a class="recruit_detail" href="rc_dt_02.php">エンジニア採用</a>
                        <a class="recruit_detail" href="rc_dt_03.php">管理スタッフ採用</a>
                        <a class="recruit_detail" href="rc_dt_04.php">アルバイト採用</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>