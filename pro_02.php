<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="pro_detail">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>Properties</h1>
                    <div class="in_tx">
                        <div class="detail">
                            <h3>横浜市旭区（二俣川駅）</h3>
                            <img src="img/img_buil01.jpg" alt="">
                        </div>
                        <dl class="pro_detail_text">
                            <dt>住所表示</dt>
                            <dd>神奈川県横浜市四季美台68番41</dd>
                            <dt>地番</dt>
                            <dd>横浜市旭区四季美台68番41.40.43</dd>
                            <dt>権利形態</dt>
                            <dd>所有権</dd>
                            <dt>接面道路</dt>
                            <dd>西側約2mが幅員4.5mの公道に接道</dd>
                            <dt>私道負担</dt>
                            <dd>無</dd>
                            <dt>種類</dt>
                            <dd>住宅</dd>
                            <dt>構造</dt>
                            <dd>木造ストレート茸2階建</dd>
                            <dt>床面積</dt>
                            <dd>1階49.99㎡　2階40.07㎡（合計90.06㎡）</dd>
                            <dt>築年数</dt>
                            <dd>平成12年10月</dd>
                            <dt>その他</dt>
                            <dd>都市緑地法等</dd>
                            <dt>区域区分</dt>
                            <dd>市街化区域</dd>
                            <dt>地域地区</dt>
                            <dd>第1種低層住居専用地域・第1種高度地区</dd>
                            <dt>建ぺい率</dt>
                            <dd>50％</dd>
                            <dt>容積率</dt>
                            <dd>80％</dd>
                            <dt>その他</dt>
                            <dd>◆バリアフリー仕様<br>浴室移動用リフト・車いす昇降機・車いす対応洗面台<br>◆状況　空室<br>◆引渡　即<br>◆内見　可</dd>
                        </dl>

                    </div>
                </div>

                <a href="properties.php">
                    << Properties ALL view</a>
            </div>
        </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>