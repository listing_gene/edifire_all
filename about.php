<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
  <meta name="format-detection" content="telephone=no">
  <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
  <title>株式会社EDIFIRE</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/header-footer.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <link href="css/reboot.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200&display=swap" rel="stylesheet">
</head>

<body>
  <!-- all common header -->
  <?php include('./header.html'); ?>
  <!-- all common header -->

  <main>
    <div id="wrap" class="about_content">
      <div class="inner">
        <!-- <div class="about_box conte_bg02 frame">
          <h1>ABOUT</h1>
          <div class="in_tx02">
            <p>私たちは不動産投資に変革をもたらし、不動産投資を再定義し、<br class="pc">
              ユーザー利益を最大化、お客様に素晴らしい体験を提供します。<br class="pc">
              私たちは、不動産投資に特化したスタートアップ企業です。<br class="pc">
              私たちの使命は、お客様に投資に対して可能な限り最高のリターンを提供することです。<br class="pc">
              私たちは、時間の経過とともに価値が上がると思われる物件を慎重に選ぶことでこれを実現しています。
            </p>
          </div>
          <div class="bc">
              <img src="./img/BC_01_01.svg" alt="">
              <p>安心できる資産</p>
              <p>不動産投資の始め方から、<br class="pc">
                あなたに合わせた<br class="pc">
                資産創りをご提案致します。</p>
            </div>
            <div class="bc">
              <img src="./img/BC_01_02.svg" alt="">
              <p>所有と運営</p>
              <p>不動産の大きな役割として<br class="pc">
                ”所有と運営”のうち、<br class="pc">
                所有されているご資産を当社が運営。<br class="pc">
                最大限の価値を引き出すトータルサポート。</p>
            </div>
          <div class="bc_wrap inline">
            <p>不動産流通プラットフォームの運営</p>
            <div class="bc">
              <p>問題点</p>
              <p>私たちが解決する問題は、多くの人が不動産投資から最大限の利益を得ることができていないことです。彼らは、適切な物件を選択するための時間や知識を持っていないかもしれません。</p>
            </div>
            <div class="bc">
              <p>問題解決</p>
              <p>私たちは、お客様が十分な情報を得た上で投資に関する意思決定を行うために必要なリソースと知識を提供することで、この問題を解決しています。また、私たちが物件を管理することで、お客様は日々の投資管理に煩わされることなく、ご自身の生活に専念することができます。
              </p>
            </div>
          </div>
          <div class="bc_wrap inline">
            <p>リノベーションに関するデザイン、設計、請負</p>
            <div class="bc">
              <p>マーケット</p>
              <p>私たちがターゲットとする市場は不動産投資市場です。これには不動産への投資を考えている個人、機関、企業が含まれます。</p>
            </div>
            <div class="bc">
              <p>戦略</p>
              <p>私たちの戦略は、口コミでビジネスを拡大することです。お客様に素晴らしい体験を提供し、友人や家族に喜んで紹介していただけるようにします。</p>
            </div>
          </div>
        </div> -->
        <div class="conte_bg frame about_bg">
          <h1>About EDIFIRE</h1>
          <div class="about_text">
            <p>私たちは不動産投資に変革をもたらし、不動産投資を再定義し、<br class="pc">
              ユーザー利益を最大化、お客様に素晴らしい体験を提供します。<br class="pc">
              私たちは、不動産投資に特化したスタートアップ企業です。<br class="pc">
              私たちの使命は、お客様に投資に対して可能な限り最高のリターンを提供することです。<br class="pc">
              私たちは、時間の経過とともに価値が上がると思われる物件を慎重に選ぶことで、これを実現しています。
            </p>
          </div>
          <div class="bc_wrap inline">
            <!-- <p>不動産流通プラットフォームの運営</p> -->
            <div class="bc about_item">
              <p>課題点</p>
              <p>私たちが解決する問題は、多くの人が不動産投資から最大限の利益を得ることができていないことです。彼らは、適切な物件を選択するための時間や知識を持っていないかもしれません。</p>
            </div>
            <div class="bc about_item">
              <p>課題解決</p>
              <p>私たちは、お客様が十分な情報を得た上で投資に関する意思決定を行うために必要なリソースと知識を提供することで、この問題を解決しています。私たちが物件を管理することで、お客様は日々の投資管理に煩わされることなくご自身の生活に専念することができます。
              </p>
            </div>
          </div>
          <div class="bc_wrap inline">
            <!-- <p>リノベーションに関するデザイン、設計、請負</p> -->
            <div class="bc about_item">
              <p>マーケット</p>
              <p>私たちがターゲットとする市場は不動産投資市場です。これには、不動産への投資を考えている個人・機関・企業が含まれます。</p>
            </div>
            <div class="bc about_item">
              <p>戦略</p>
              <p>私たちの戦略は、口コミでビジネスを拡大することです。お客様に素晴らしい体験を提供し、友人や家族に喜んで紹介していただけるようにします。</p>
            </div>
          </div>
          <!-- <div class="in_tx">
            <div class="service_text">
              <p>問題点</p>
              <p>私たちが解決する問題は、多くの人が不動産投資から最大限の利益を得ることができていないことです。彼らは、適切な物件を選択するための時間や知識を持っていないかもしれません。</p>
            </div>
          </div>
          <div class="in_tx">
            <div class="service_text">
              <p>問題解決</p>
              <p>私たちは、お客様が十分な情報を得た上で投資に関する意思決定を行うために必要なリソースと知識を提供することで、この問題を解決しています。また、私たちが物件を管理することで、お客様は日々の投資管理に煩わされることなく、ご自身の生活に専念することができます。</p>
            </div>
          </div>
          <div class="in_tx">
            <div class="service_text">
              <p>マーケット</p>
              <p>私たちがターゲットとする市場は、不動産投資市場です。これには、不動産への投資を考えている個人、機関、企業が含まれます。</p>
            </div>
          </div>
          <div class="in_tx">
            <div class="service_text">
              <p>戦略</p>
              <p>私たちの戦略は、口コミでビジネスを拡大することです。お客様に素晴らしい体験を提供し、友人や家族に喜んで紹介していただけるようにします。</p>
            </div>
          </div> -->
          <!-- <p>人の役に立つ仕事をしたいと考え、社会に出て一番最初の職業は、<br>
              わかりやすく東京消防庁での消防士という職業を選びました。</p>
            <p> 人命救助というミッションは共感できるものであり、<br>
              誇り高き仕事だと思っていましたが、東日本大震災を経験し、官という枠組みに窮屈さを感じ退職しました。</p>
            <p> 1年ほど語学の勉強をし、バーラウンジなどで働きながら、<br>
              自分の人生をどうしていきたいか考え、<br class="pc">次は不動産ベンチャー企業に入社して、その会社が上場するまで在籍させて頂きました。</p>
            <p>しかし、会社が大きくなるにつれ公務員の時に感じた窮屈さを感じるようになり、起業しました。</p>
            <p>少ない経験ではありますが、公務員や上場企業での枠組みの中では、<br>
              常日頃から自分の考えてきた「人の役に立つ仕事」を組み立てていくことは難しいと感じていたので、<br>
              次に起業を考えたのは自然な流れでした。</p> -->
        </div>
      </div>
    </div>
    <!-- <div class="inner frame ">
        <div class="in_tx02">
          <p>一番長く働いてきた不動産業界にも旧態依然した枠組みは数多く残されています。<br>
            ユーザーメリットよりも、 業界慣習の方が優先されることも多く、<br>
            改善していかなければならないと感じました。</p>
          <p>従来の枠組みの良さも当然の事ながら感じてきましたが、<br>
            本質と乖離してきている部分など数多くあり、 <br>
            時代に最適化した方が全体最適になることも多くあります。</p>
          <p>今までの仕事の中で、業界など変えたりしてみましたが、<br>
            私の生きる原動力は、”人の役に立ちたい”ただそれだけです。</p>
          <p>地位や名誉、肩書や世間体などに全く興味はありません。<br>
            レールに敷かれた人生ではなく、自分の頭で考え行動して、<br>
            働き方というものも再定義し、<br>
            EDIFIREを通して、みなさまにユーザーファーストなサービスを提供していき、<br>
            世の中に価値を創造する会社にします。 </p>
          <p>株式会社EDIFIRE<br>
            代表取締役　荒関 勇人</p>
        </div>
      </div> -->
    <div class="inner">
      <div class="conte_bg03 frame ">
        <h2>会社概要</h2>
        <div class="content_company">
          <table>
            <dl>
              <dt>会社名</dt>
              <dd>
                <p>株式会社EDIFIRE</p>
              </dd>
            </dl>

            <dl>
              <dt>設立</dt>
              <dd>
                <p>2016年10月12日</p>
              </dd>
            </dl>

            <dl>
              <dt>資本金</dt>
              <dd>
                <p>2,000万円</p>
              </dd>
            </dl>

            <dl>
              <dt>代表取締役</dt>
              <dd>
                <p>荒関 勇人</p>
              </dd>
            </dl>

            <dl>
              <dt>所在地</dt>
              <dd>
                <p>〒106-0032<br>
                  東京都港区六本木5-2-1　ほうらいやビル5階<br>
                  <a href="tel:03-5544-8522"><i class="fas fa-phone fa-fw"></i>03-5544-8522</a><br>
                  <a href="mailto:info@edifire.co.jp"><i class="far fa-paper-plane fa-fw"></i>info@edifire.co.jp</a>
                </p>
              </dd>
            </dl>

            <dl>
              <dt>従業員数</dt>
              <dd>
                <p>10名(アルバイト含む）</p>
              </dd>
            </dl>

            <dl>
              <dt>事業内容</dt>
              <dd>
                <p>不動産投資に関するコンサルティング事業<br>
                  不動産販売及び買取<br>
                  マンションの賃貸管理
                </p>
              </dd>
            </dl>

            <dl>
              <dt>法律顧問</dt>
              <dd>
                <p>ソシアス総合法律事務所</p>
              </dd>
            </dl>

            <dl>
              <dt>税務顧問</dt>
              <dd>
                <p>吉田会計事務所</p>
              </dd>
            </dl>

            <dl>
              <dt>宅地建物取引業者</dt>
              <dd>
                <p>東京都知事(2) 第99943号</p>
              </dd>
            </dl>

            <dl>
              <dt>賃貸住宅管理業者</dt>
              <dd>
                <p>国土交通大臣(01) 第001016号</p>
              </dd>
            </dl>


          </table>
        </div>
      </div>
      <div class="inner">
        <div class="conte_bg04 frame ">
          <h2 class="wh">アクセス</h2>
          <div class="in_tx access_box">
            <div class="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.593746946677!2d139.73093487649246!3d35.66237957259326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b9d14147c63%3A0x857d869d7cbdf50!2z44CSMTA2LTAwMzIg5p2x5Lqs6YO95riv5Yy65YWt5pys5pyo77yV5LiB55uu77yS4oiS77yRIOOBu-OBhuOCieOBhOOChOODk-ODqyA16ZqO!5e0!3m2!1sja!2sjp!4v1685434309307!5m2!1sja!2sjp"  width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen loading="lazy"></iframe>
              <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.798952625559!2d139.7348402154457!3d35.65732468888967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b9f348edd6d%3A0x8b947ef73c2312e4!2zVk9SVOm6u-W4g-WNgeeVqg!5e0!3m2!1sja!2sjp!4v1626684614389!5m2!1sja!2sjp" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen loading="lazy"></iframe> -->
            </div>
            <div class="access_text">
              <p>株式会社EDIFIRE（EDIFIRE Inc.）</p>
              <p>〒106-0032<br>
                東京都港区六本木5-2-1　ほうらいやビル5階<br>
                03-5544-8522</p>
            </div>
          </div>
        </div>
      </div>
      <div class="inner">
        <div class="conte_bg05 frame">
          <h2>採用情報</h2>
          <a class="recruit" href="recruit.php">詳細はこちらから</a>
        </div>
      </div>
    </div>
    </div>
  </main>

  <!-- all common footer-->
  <?php include('./footer.html'); ?>
  <!-- all common footer-->
  <script>
    $(function() {
      var height = $("#header").height();
      $("body").css("margin-top", height + 10);
    });
    $(function() {
      $('.btn-trigger').on('click', function() {
        $(this).toggleClass('active');
        return false;
      });
    });
    $(function() {
      var $btn = $('.btn-trigger');
      $('#NavArea').click, $btn.click(function() {
        $(this).toggleClass('open');
        if ($(this).hasClass('open')) {
          $('#mask').addClass('open'),
            $('.hamberger').addClass('open');
        } else {
          $('#mask').removeClass('open'),
            $('.hamberger').removeClass('open');
        }
      });
    });
  </script>
  <script>
    $(window).on('load', function() {
      $("#LOADER-BG").delay(2000).fadeOut(1300);
    });
  </script>
</body>

</html>