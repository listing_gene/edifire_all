<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="pro_detail">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>Properties</h1>
                    <div class="in_tx">
                        <div class="detail">
                            <h3>ラグジュアリーアパートメント三田慶大前</h3>
                            <img src="img/img_buil02.jpg" alt="">
                        </div>
                        <dl class="pro_detail_text">
                            <dt>家賃</dt>
                            <dd>96,000円/月</dd>
                            <dt>管理費</dt>
                            <dd>7,470円/月</dd>
                            <dt>積立金</dt>
                            <dd>6,730円/月</dd>
                            <dt>所在地</dt>
                            <dd>港区芝5-2-4</dd>
                            <dt>アクセス</dt>
                            <dd>都営浅草線「三田」駅　徒歩5分<Br>
                                山手線「田町」駅　徒歩7分</dd>
                            <dt>土地権利</dt>
                            <dd>所有権</dd>
                            <dt>構造</dt>
                            <dd>鉄筋コンクリート造8階建　4階部分</dd>
                            <dt>専有面積</dt>
                            <dd>22.78㎡</dd>
                            <dt>バルコニー</dt>
                            <dd>4.69㎡</dd>
                            <dt>総戸数</dt>
                            <dd>36戸</dd>
                            <dt>築年数</dt>
                            <dd>平成16年1月</dd>
                            <dt>施工会社</dt>
                            <dd>株式会社イチケン</dd>
                            <dt>施主</dt>
                            <dd>株式会社エスグラントコーポレーション</dd>
                            <dt>管理会社</dt>
                            <dd>グローブシップ株式会社</dd>
                            <dt>現状</dt>
                            <dd>賃貸中</dd>
                            <dt>引渡</dt>
                            <dd>相談</dd>
                        </dl>
                    </div>
                </div>
                <a href="properties.php">
                    << Properties ALL view</a>
            </div>
        </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>