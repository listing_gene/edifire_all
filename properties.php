<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200&display=swap" rel="stylesheet">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="properties_content">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>Properties</h1>
                    <div class="in_tx">
                        <h4>Comming Soon…</h4>
                        <!-- <ul class="pro_box">
                            <li>
                                <a href="./pro_02.php"><img src="img/img_buil01.jpg" alt=""></a>
                                <p class="buil_name">ラグジュアリーアパートメント<br>三田慶大前</p>
                            </li>
                            <li>
                                <a href="./pro_01.php"><img src="img/img_buil02.jpg" alt=""></a>
                                <p class="buil_name">横浜市旭区（二俣川駅）</p>
                            </li>
                            <li>
                                <a href=""><img src="img/img_buil01.jpg" alt=""></a>
                                <p class="buil_name">ラグジュアリーアパートメント<br>三田慶大前</p>
                            </li>
                            <li>
                                <a href=""><img src="img/img_buil02.jpg" alt=""></a>
                                <p class="buil_name">横浜市旭区（二俣川駅）</p>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>