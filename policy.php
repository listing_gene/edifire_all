<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200&display=swap" rel="stylesheet">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>
        <div id="wrap" class="service_content">
            <div class="inner policy">
                <div class="conte_bg frame">
                    <h1>PRIVACY POLICY</h1>
                    <h2>プライバシーポリシー</h2>
                    <p>株式会社EDIFIRE（以下「当社」）は、以下のとおり個人情報保護方針を定め、個人情報保護の仕組みを構築し、全従業員に個人情報保護の重要性の認識と取組みを徹底させることにより、個人情報の保護を推進致します。</p>
                    <div class="in_tx">
                        <p>個人情報の管理<br>
                            当社は、お客さまの個人情報を正確かつ最新の状態に保ち、個人情報への不正アクセス・紛失・破損・改ざん・漏洩などを防止するため、セキュリティシステムの維持・管理体制の整備・社員教育の徹底等の必要な措置を講じ、安全対策を実施し個人情報の厳重な管理を行ないます。</p>
                        <p>個人情報の利用目的<br>
                            お客さまからお預かりした個人情報は、当社からのご連絡や業務のご案内やご質問に対する回答として、電子メールや資料のご送付に利用いたします。</p>
                        <p>個人情報の第三者への開示・提供の禁止<br>
                            当社は、お客さまよりお預かりした個人情報を適切に管理し、次のいずれかに該当する場合を除き、個人情報を第三者に開示いたしません。
                            •お客さまの同意がある場合
                            •お客さまが希望されるサービスを行なうために当社が業務を委託する業者に対して開示する場合
                            •法令に基づき開示することが必要である場合</p>
                        <p>個人情報の安全対策<br>
                            当社は、個人情報の正確性及び安全性確保のために、セキュリティに万全の対策を講じています。ご本人の照会お客さまがご本人の個人情報の照会・修正・削除などをご希望される場合には、ご本人であることを確認の上、対応させていただきます。</p>
                        <p>法令、規範の遵守と見直し<br>
                            当社は、保有する個人情報に関して適用される日本の法令、その他規範を遵守するとともに、本ポリシーの内容を適宜見直し、その改善に努めます。</p>
                        <p>お問い合わせ<br>
                            当社の個人情報の取扱に関するお問い合せは下記までご連絡ください。<br>
                            〒106-0032<br>
                            東京都港区六本木5-2-1　ほうらいやビル5階<br>
                            TEL:03-5544-8522 FAX:03-6831-2773<br>
                            株式会社EDIFIRE<br>
                            個人情報保護管理者　鈴木<br>
                        </p>
                        <a class="contact" href="contact.php">お問い合わせ <i class="far fa-paper-plane fa-fw"></i></a>
                    </div>
                </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>