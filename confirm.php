<?php
// フォームのボタンが押されたら
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // フォームから送信されたデータを各変数に格納
    $name = $_POST["name"];
    $mail = $_POST["mail"];
    $tel = $_POST["tel"];
    $purpose = $_POST["purpose"];
    $content  = $_POST["content"];
}

// 送信ボタンが押されたら
if (isset($_POST["submit"])) {

    // 下から受付メール
    // 送信ボタンが押された時に動作する処理をここに記述する
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    // 件名を変数subjectに格納
    $subject = "【お問い合わせ】株式会社EDIFIRE";
    // メール本文を変数bodyに格納
    $body = <<< EOM

株式会社EDIFIREホームページからお問い合わせがありました。

=================================


【 お名前 】
{$name}

【 メールアドレス 】
{$mail}

【 電話番号 】
{$tel}

【 お問い合わせ項目 】
{$purpose}

【 お問い合わせ内容 】
{$content}

=================================

EOM;

    // 送信元のメールアドレスを変数fromEmailに格納
    $fromEmail = "$mail";
    $Email = "info@edifire.co.jp";
    // 送信元の名前を変数fromNameに格納
    $fromName = "株式会社EDIFIRE";
    // ヘッダ情報を変数headerに格納する
    $header = "Content-Type:text/html;charset=UTF-8\r\n";
    $header .= "From: info@edifire.co.jp\r\n";
    $header .= "Return-Path: info@edifire.co.jp\r\n";
    $param = "-f info@edifire.co.jp";
    $header = "From: " . mb_encode_mimeheader($name) . "<{$mail}>";
    // メール送信を行う
    mb_send_mail($Email, $subject, $body, $header);

    // 送信ボタンが押された時に動作する処理をここに記述する
    mb_language("ja");
    mb_internal_encoding("UTF-8");

    // 件名を変数subjectに格納
    $subject = "【お問い合わせ】株式会社EDIFIRE";

    // メール本文を変数bodyに格納
    $body = <<< EOM
{$name}　様

お問い合わせありがとうございます。
以下のお問い合わせ内容を、メールにて確認させていただきました。

=================================

【 お名前 】
{$name}

【 メールアドレス 】
{$mail}

【 電話番号 】
{$tel}

【 お問い合わせ項目 】
{$purpose}

【 お問い合わせ内容 】
{$content}

=================================

内容を確認のうえ、回答させて頂きます。
しばらくお待ちください。
EOM;

    // 送信元のメールアドレスを変数fromEmailに格納
    $fromEmail = "info@edifire.co.jp";

    // 送信元の名前を変数fromNameに格納
    $fromName = "株式会社EDIFIRE";

    // ヘッダ情報を変数headerに格納する
    $header = "Content-Type:text/html;charset=UTF-8\r\n";
    $header .= "From: info@edifire.co.jp\r\n";
    $header .= "Return-Path: info@edifire.co.jp\r\n";
    $param = "-f info@edifire.co.jp";
    $header = "From: " . mb_encode_mimeheader($fromName) . "<{$fromEmail}>";

    // メール送信を行う
    //mb_send_mail("kanda.it.school.trial@gmail.com", "メール送信テスト", "メール本文");
    mb_send_mail($mail, $subject, $body, $header);

    // サンクスページに画面遷移させる
    header("Location: ./thanks.php");
    exit;
}
?>


<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200&display=swap" rel="stylesheet">
</head>

<body>
    <!-- all common header -->
    <?php include('./header.html'); ?>
    <!-- all common header -->

    <main>

        <div id="wrap" class="contact_content">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>CONTACT</h1>
                    <div class="in_tx Form-Box">
                        <form action="confirm.php" method="post">
                            <input type="hidden" name="company" value="<?php echo $company; ?>">
                            <input type="hidden" name="name" value="<?php echo $name; ?>">
                            <input type="hidden" name="mail" value="<?php echo $mail; ?>">
                            <input type="hidden" name="tel" value="<?php echo $tel; ?>">
                            <input type="hidden" name="purpose" value="<?php echo $purpose; ?>">
                            <input type="hidden" name="content" value="<?php echo $content; ?>">
                            <h2>ご入力内容の確認</h2>
                            <p class="text contact_text">
                                入力内容にお間違いがないか確認の上、<br class="sp">「送信」ボタンを押して下さい。<br>
                                再度ご入力される場合は「戻る」ボタンを押して下さい。</p>
                            <div class="inner contact_text">
                                <div>
                                    <label>お名前</label>
                                    <p><?php echo $name; ?></p>
                                </div>
                                <div>
                                    <label>メールアドレス</label>
                                    <p><?php echo $mail; ?></p>
                                </div>
                                <div>
                                    <label>電話番号</label>
                                    <p><?php echo $tel; ?></p>
                                </div>
                                <div class="purpose">
                                    <label>お問い合わせ項目</label>
                                    <p><?php echo $purpose; ?></p>
                                </div>
                                <div>
                                    <label>お問い合わせ内容</label>
                                    <p><?php echo $content; ?></p>
                                </div>
                            </div>
                            <div class="btn-group  contact_text">
                                <label for="content">
                                    <input type="button" class="Form-Btn contact-btn" value="戻る" id="back" onclick="history.back(-1)">
                                </label>
                                <label for="content" class="">
                                    <input type="submit" name="submit" class="Form-Btn contact-btn" value="送信">
                                </label>
                                <!-- <input type="button" value="戻る" id="back" onclick="history.back(-1)"> -->
                                <!-- <button type="submit" name="submit">送信する</button> -->
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- all common footer-->
    <?php include('./footer.html'); ?>
    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>