<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="[株式会社EDIFIRE]不動産投資に関するコンサルティング事業,不動産販売及び買取,マンションの賃貸管理">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="不動産投資,不動産販売,不動産買取,マンションの賃貸管理">
    <title>株式会社EDIFIRE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/91317bd0bf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/header-footer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/reboot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/tpw8rsz.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=Montserrat:wght@200;400&display=swap" rel="stylesheet">
</head>

<body>
    <!-- all common header -->

    <header>
        <div class="header_content">
            <div class="head_logo">
                <a href="index.php"><img src="./img/logo_edifire.png" alt=""></a>
            </div>
            <div class="head_menu">
                <ul>
                    <li><a href="about.php">About us</a></li>
                    <li><a href="service.php">Service</a></li>
                    <li><a href="properties.php">Properties</a></li>
                    <li><a href="recruit.php">Recruit</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </div>
            <div class="hamberger sp">
                <nav>
                    <div class="nav_inner">
                        <ul>
                            <li><a href="about.php">About us</a></li>
                            <li><a href="service.php">Service</a></li>
                            <li><a href="properties.php">Properties</a></li>
                            <li><a href="recruit.php">Recruit</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <section>
                    <div class="btn-trigger" id="btn02">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </section>
                <div id="mask"></div>
            </div>
        </div>
    </header>

    <main>
        <div id="wrap" class="contact_content">
            <div class="inner">
                <div class="conte_bg frame">
                    <h1>Contact</h1>
                    <div class="in_tx">
                        <h2>お問い合わせはこちらから</h2>
                        <div class="contact_wrap">
                            <form action="confirm.php" class="Form" method="post" name="form" onsubmit="return validate()">
                                <div class="Form-Box">
                                    <label for="name" class="Form-Item-Label Form-Item">
                                        <p class="Form-Item-Label">お名前&nbsp;<span class="red-color"></span></p>
                                        <input id="name" type="text" class="Form-Item-Input" name="name" placeholder="例）山田&nbsp;太郎" value="">
                                    </label>
                                    <label for="tel" class="Form-Item-Label Form-Item">
                                        <p class="Form-Item-Label">電話番号&nbsp;<span style="font-size: 0.9rem; color: #1f8996;">半角数字ハイフンなし</span></p>
                                        <input id="tel" type="text" class="Form-Item-Input" name="tel" placeholder="例）01201111111" value="">
                                    </label>
                                    <label for="mail" class="Form-Item-Label Form-Item">
                                        <p class="Form-Item-Label">メールアドレス<span class="red-color">&nbsp;</span></p>
                                        <input id="mail" type="text" class="Form-Item-Input" name="mail" placeholder="例）info@mail.com" value="">
                                    </label>
                                    <label for="name" class="Form-Item-Label Form-Item">
                                        <p class="Form-Item-Label">お問い合わせ項目</p>
                                        <div class="Form-Item-purpose">
                                            <input id="tenant" type="radio" name="purpose" value="当社管理物件の入居者様" checked>
                                            <label for="tenant">当社管理物件の入居者様</label><br>
                                            <input id="owner" type="radio" name="purpose" value="当社管理物件の所有者様">
                                            <label for="owner">当社管理物件の所有者様</label><br>
                                            <input id="recruit" type="radio" name="purpose" value="採用に関すること">
                                            <label for="recruit">採用に関すること</label><br>
                                            <input id="other" type="radio" name="purpose" value="その他">
                                            <label for="other">その他</label>
                                        </div>
                                    </label>
                                    <label for="content" class="Form-Item-Label Form-Item">
                                        <p class="Form-Item-Label">お問い合わせ内容<span class="red-color">&nbsp;</span></p>
                                        <textarea name="content" class="Form-Item-Textarea" rows="7" placeholder="お問い合わせ内容をご入力ください。"></textarea>
                                    </label>
                                    <label for="content" class="Form-Item-Label Form-Item">
                                        <input type="submit" class="Form-Btn" value="確認画面へ">
                                    </label>
                                    <!-- <div class="Form-Item">
                                        <input type="submit" class="Form-Btn" value="確認画面へ">
                                    </div> -->
                                    <!-- <button type="submit">確認画面へ</button> -->
                                </div>
                            </form>

                            <!-- <div class="Form">
                                <div class="Form-Item">
                                    <p class="Form-Item-Label">お名前</p>
                                    <input type="text" class="Form-Item-Input" placeholder="例）example">
                                </div>
                                <div class="Form-Item">
                                    <p class="Form-Item-Label">電話番号</p>
                                    <input type="text" class="Form-Item-Input" placeholder="例）000-0000-0000">
                                </div>
                                <div class="Form-Item">
                                    <p class="Form-Item-Label">メールアドレス</p>
                                    <input type="email" class="Form-Item-Input" placeholder="例）example@example.com">
                                </div>
                                <div class="Form-Item">
                                    <p class="Form-Item-Label">お問い合わせ項目</p>
                                    <div class="Form-Item-purpose">
                                        <input id="tenant" type="radio" name="purpose" value="当社管理物件の入居者様" checked>
                                        <label for="tenant">当社管理物件の入居者様</label><br>

                                        <input id="owner" type="radio" name="purpose" value="当社管理物件の所有者様">
                                        <label for="owner">当社管理物件の所有者様</label><br>

                                        <input id="recruit" type="radio" name="purpose" value="採用に関すること">
                                        <label for="recruit">採用に関すること</label><br>

                                        <input id="other" type="radio" name="purpose" value="その他">
                                        <label for="other">その他</label>
                                    </div>
                                </div>
                                <div class="Form-Item">
                                    <p class="Form-Item-Label">お問い合わせ内容</p>
                                    <textarea class="Form-Item-Textarea" placeholder="こちらにお問い合わせ内容をご記入ください。"></textarea>
                                </div>
                                <input type="submit" class="Form-Btn" value="送信">
                            </div> -->
                            <div class="contact_pl">
                                <h3>お問い合わせに関する個人情報のお取扱いについて</h3>
                                <p><b>事業者名称</b>　株式会社EDIFIRE<br>
                                    <b>個人情報保護管理者</b>　鈴木<br>
                                    <b>問い合わせ先電話番号</b>　<a href="tel:03-5544-8522">03-5544-8522</a>
                                </p>
                                <p class="pl_dt">
                                <h5>【個人情報の利用目的】</h5>
                                ご入力いただいた個人情報は、お問合せ対応のために利用致します。
                                個人情報の第三者提供について
                                本人の同意がある場合又は法令に基づく場合を除き、取得した個人情報を第三者に提供することはありません。
                                個人情報の取扱いの委託について
                                取得した個人情報の取扱いの全部又は一部を委託することはありません。
                                </p>
                                <p class="pl_dt">
                                <h5>【開示対象個人情報の開示等および問い合わせ窓口について】</h5>
                                ご本人からの求めにより、当社が保有する開示対象個人情報の利用目的の通知・開示・内容の訂正・追加または
                                削除・利用の停止・消去および第三者への提供の停止（「開示等」といいます。）に応じます。
                                </p>
                                <p class="pl_dt">
                                <h5>【個人情報を入力するにあたっての注意事項】</h5>
                                連絡先の入力は任意となっております。
                                連絡先をご入力いただけない場合は、お問い合わせの回答に応じることができませんので、
                                あらかじめご了承下さい。
                                </p>
                                <p class="pl_dt">
                                <h5>【本人が容易に認識できない方法による個人情報の取得】</h5>
                                クッキーやウェブビーコン等を用いるなどして、
                                本人が容易に認識できない方法による個人情報の取得は行っておりません。
                                </p>
                                <p class="pl_dt">
                                <h5>【個人情報の安全管理措置について】</h5>
                                取得した個人情報については、漏洩、減失またはき損の防止と是正、その他個人情報の安全管理のために必要かつ
                                適切な措置を講じ、応対品質向上を目的としてお問合せへの回答後も、
                                取得した個人情報は当社内において適切に保管致します。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="footer_content">
            <!-- <div class="foot_logo">
                <a href="index.php"><img src="./img/logo_edifire.png" alt=""></a>
            </div> -->
            <div class="foot_official">
                <ul>
                    <li>企業概要</li>
                    <li>株式会社EDIFIRE</li>
                    <li><i class="fas fa-map-marker-alt fa-fw"></i>〒106-0032</li>
                    <li>東京都港区六本木5-2-1　ほうらいやビル5階</li>
                    <li><i class="fas fa-phone fa-fw"></i>03-5544-8522</li>
                    <li><i class="far fa-paper-plane fa-fw"></i>info@edifire.co.jp</li>
                </ul>
            </div>
            <div class="foot_site">
                <ul>
                    <li>サイトメニュー</li>
                    <li><a href="about.php">会社概要</a></li>
                    <li><a href="service.php">事業内容</a></li>
                    <li><a href="properties.php">所有物件</a></li>
                    <li><a href="recruit.php">採用情報</a></li>
                    <!-- <li><a href="#">EDIFIREプラットフォーム</a></li> -->
                    <li><a href="policy.php">プライバシーポリシー</a></li>
                    <li><a href="contact.php">お問い合わせ</a></li>
                </ul>
            </div>
            <div class="foot_sns">
                <ul>
                    <li>
                        <a href="https://www.facebook.com/Edifire-Inc-1780084415607714/">
                            <span class="fb"><i class="fab fa-facebook-f"></i></span>
                        </a>
                    </li>
                    <li>
                        <span class="twitter"><i class="fab fa-twitter"></i></span>
                    </li>
                    <li>
                        <span class="insta"><i class="fab fa-instagram"></i></span>
                    </li>
                </ul>
            </div>
        </div>
    </footer>

    <!-- all common footer-->
    <script>
        $(function() {
            var height = $("#header").height();
            $("body").css("margin-top", height + 10);
        });
        $(function() {
            $('.btn-trigger').on('click', function() {
                $(this).toggleClass('active');
                return false;
            });
        });
        $(function() {
            var $btn = $('.btn-trigger');
            $('#NavArea').click, $btn.click(function() {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $('#mask').addClass('open'),
                        $('.hamberger').addClass('open');
                } else {
                    $('#mask').removeClass('open'),
                        $('.hamberger').removeClass('open');
                }
            });
        });
    </script>
    <script>
        $(window).on('load', function() {
            $("#LOADER-BG").delay(2000).fadeOut(1300);
        });
    </script>
</body>

</html>